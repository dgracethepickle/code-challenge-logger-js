window.Logger = function(initDefaultMessage, initLogFunction) {
    var logFunction = function(st) { console.log(st); };
    var defaultMessage = 'Event happened';

    if (typeof initLogFunction == 'function') {
        logFunction = initLogFunction;
    } else {
        if (initLogFunction !== null && initLogFunction !== undefined) {
            logFunction('Invalid log function provided. Using console.log instead.');
        }
    }

    if (typeof initDefaultMessage == 'string') {
        defaultMessage = initDefaultMessage;
    } else {
        if (initDefaultMessage !== null && initDefaultMessage !== undefined) {
            logFunction('Default message is not a string, using "' + defaultMessage + '" instead.');
        }
    }

    return {
        changeDefaultMessage: function(st) {
            if (typeof st == 'string') {
                defaultMessage = st;
                return true;
            } else {
                logFunction('Attempted to set default message to a non-string. Rejected.');
                return false;
            }
        },
        changeLogFunction: function(f) {
            if (typeof f == 'function') {
                logFunction = f;
                return true;
            } else {
                logFunction('Attempted to set default log function to a non-function. Rejected.');
                return false;
            }
        },
        log: function(st) {
            if (typeof st == 'string') {
                logFunction(st);
                return true;
            } else if (st === null || st === undefined) {
                logFunction(defaultMessage);
                return true;
            } else {
                logFunction('Attempted to call log with a non-string non-null value.');
                return false;
            }
        }
    }
}