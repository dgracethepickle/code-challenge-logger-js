# User

This code challenge was completed by Daniel Grace (dgrace@gracefulcode.com)

# Requirements
Create a extendable Logger function in JavaScript. The Logger function should provide means to register new logging methods and configurable to set a default message and default logger. The Logger function should also be unit tested. 

In a code example, show how to extend the Logger by registering the following three logging methods to an instantiated Logger.

1. console.log(msg)
2. alert(msg)
3. document.write(msg)

This challenge is looking for a clear understanding of extendability and configurability.

## Workflow

This should not be completed using the bit bucket web interface. 

* Fork this repository
* Create a branch to work in
* Do a pull request when your code is ready to be reviewed

Store code examples and documentation in the wiki of your repo.

All code should use spaces instead of tabs. 