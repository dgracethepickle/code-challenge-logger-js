(function() {
    var results = [];
    function assert(bool, msg) {
        console.log((bool ? 'PASS: ' : 'FAIL: ') + msg);
        results[results.length] = {
            result: bool,
            message: msg
        };
    }

    var lastLog = null;
    var logFunction = function(e) {
        lastLog = e;
    };

    var logger = new Logger('Default message', logFunction);

    var ret = logger.log('test a');
    assert(ret === true, 'Logger return value for simple message passing.');
    assert(lastLog === 'test a', 'Simple message passing');

    ret = logger.log();
    assert(ret === true, 'Logger return value for default message.');
    assert(lastLog === 'Default message', 'Default message');

    ret = logger.changeDefaultMessage('Micky mouse');
    assert(ret === true, 'Change default message return value should succeed.');

    ret = logger.log();
    assert(ret === true, 'Logger return value for default message after change.')
    assert(lastLog === 'Micky mouse', 'Default message after a change.');

    logger = new Logger(null, logFunction);
    ret = logger.log();
    assert(ret === true, 'Logger return value for almost entirely default setup.');
    assert(lastLog === 'Event happened', 'New instance should not by affected by old instances.');

    // NOTE: This test is sortof outside of the test harness since it's testing things we can't (easily) intercept.
    logger = new Logger();
    ret = logger.log('This should appear in the console.');
    assert(ret === true, 'Default logger and a string should succeed.');

    ret = logger.log({});
    assert(ret === false, 'Default logger and an object literal should fail.');

    // A little something special
    // Note: Normally I'd use a templating engine here, but this is a bonus outside of scope anyway, so...

    var table = document.getElementById('results');

    for (var i in results) {
        var tr = document.createElement('tr');

        var td = document.createElement('td');
        var text = document.createTextNode(results[i].result ? 'Success' : 'Failure');
        td.appendChild(text);
        tr.appendChild(td);

        td = document.createElement('td');
        text = document.createTextNode(results[i].message);
        td.appendChild(text);
        tr.appendChild(td);

        table.appendChild(tr);
    }

    document.body.appendChild(table);

})();
